<?
$cpCfg = Zend_Registry::get('cpCfg');
$tv = Zend_Registry::get('tv');

/* IR8A DETAILS IN FOOTER */
$cpCfg['cp.ir8aForm.employerAddress'] = $cpCfg['cp.addressPdf1'] .' ' . $cpCfg['cp.addressPdf2'] . ' ' . $cpCfg['cp.addressPdf4'];
$cpCfg['cp.ir8aForm.authorisedPerson'] = "MOHAMED AMEEN S/O DAWOOD";
$cpCfg['cp.ir8aForm.designation'] = "DIRECTOR";
$cpCfg['cp.ir8aForm.telephone'] = "67429527";
$cpCfg['cp.ir8aForm.date'] = "26/02/2021";
$cpCfg['cp.ir8aForm.emailAddress'] = "alansar@singnet.com.sg";
$cpCfg['cp.ir8aForm.aisFileType'] = "O"; // O - Original || A - Amendment

$dashboard = getCPModuleObj('payroll_dashboard')->model;

$arr = array();
$arr[] = $dashboard->getDasboardObj('payroll_passportExpiry', array('cssClass' => 'c50l', 'subClass' => 'subcr p0 m10'));
$arr[] = $dashboard->getDasboardObj('payroll_workpermitExpiry', array('cssClass' => 'c50r', 'subClass' => 'subcr p0 mr10 mt10'));

$arr[] = $dashboard->getDasboardObj('payroll_employeeSummary', array('cssClass' => 'c33l', 'subClass' => 'subcr p0 m10 mt15'));
$arr[] = $dashboard->getDasboardObj('payroll_localWorkers', array('cssClass' => 'c33l', 'subClass' => 'subcr p0 mr10 mt10'));
$arr[] = $dashboard->getDasboardObj('payroll_foreignWorkers', array('cssClass' => 'c33l', 'subClass' => 'subcr p0 mt10 mr5'));

$cpCfg['cp.dashboardArr'] = $arr;

CP_Common_Lib_Registry::arrayMerge('cpCfg', $cpCfg);

$tv = Zend_Registry::get('tv');
array_push($tv['protSiteSpActionExceptions'], 'sendTaskUpdatesToPM');
CP_Common_Lib_Registry::arrayMerge('tv', $tv);

$fn = Zend_Registry::get('fn');
$modulesArr = Zend_Registry::get('modulesArr');
$isDeveloper = $fn->getSessionParam('isDeveloper');

if ($isDeveloper == 1) {
    $cpCfg['cp.topRooms']['admin']['modules'] = array(
         'webBasic_content'
        ,'webBasic_category'
        ,'core_valuelist'
        ,'core_setting'
        ,'core_userGroup'
        ,'core_staff'
        ,'core_translation'
    );

    $cpCfg['cp.topRooms']['payroll']['modules'] = array(
         'payroll_dashboard'
        ,'payroll_leave'
        ,'payroll_loan'
        ,'payroll_training'
        ,'payroll_employee'
        ,'payroll_jobInformation'
        ,'payroll_leavePolicy'
        ,'payroll_payrollManagement'
        ,'payroll_cPFCalculator'
    );
} else {
    $modulesArr['payroll_employee']['actBtnsList'] = array('new');
    $cpCfg['cp.topRooms']['admin']['modules'] = array(
         'core_staff'
        ,'core_valuelist'
    );

    $cpCfg['cp.topRooms']['payroll']['modules'] = array(
         'payroll_dashboard'
        ,'payroll_leave'
        ,'payroll_loan'
        ,'payroll_training'
        ,'payroll_employee'
        ,'payroll_jobInformation'
        ,'payroll_leavePolicy'
        ,'payroll_payrollManagement'
    );

    CP_Common_Lib_Registry::arrayMerge('cpCfg', $cpCfg);
}

CP_Common_Lib_Registry::arrayMerge('modulesArr', $modulesArr);
