<?
$cpCfg = array();
$cpCfg['cp.theme'] = 'Blue';
$cpCfg['cp.hasAdminOnly'] = true;
$cpCfg['cp.frameworkName'] = 'CRM Pilot';
$cpCfg['cp.version']       = '3.0';

$cpCfg['cp.topRooms'] = array(
    'payroll' => array(
         'title' => 'Payroll'
        ,'modules' => array(
              'payroll_dashboard'
             ,'payroll_leave'
             ,'payroll_loan'
             ,'payroll_training'
             ,'payroll_employee'
             ,'payroll_jobInformation'
             ,'payroll_leavePolicy'
             ,'payroll_payrollManagement'
             ,'payroll_cPFCalculator'
        )
        ,'default' => 'payroll_employee'
    )

    ,'admin' => array(
        'title' => 'Admin'
       ,'modules' => array(
             'webBasic_content'
            ,'webBasic_category'
            ,'core_valuelist'
            ,'core_setting'
            ,'core_userGroup'
            ,'core_staff'
            ,'core_translation'
       )
       ,'default' => 'core_valuelist'
    )

    ,'reports' => array(
         'title' => 'Reports'
        ,'modules' => array(
              'payroll_reports'
        )
        ,'default' => 'payroll_reports'
    )
);

/*
$hiddenModules = array(
     'common_contactLink'
    ,'common_testRecipientLink'
    ,'common_interestLink'
    ,'enggCrm_contactLink'
    ,'enggCrm_projectLink'
    ,'enggCrm_invoiceLink'
    ,'enggCrm_opportunityLink'
    ,'enggCrm_companyAddressLink'
    ,'enggCrm_companyLink'
    ,'core_staff'
    ,'core_staffLink'
    ,'enggCrm_taskLink'
    ,'enggCrm_scheduleLink'
    ,'enggCrm_thirdPartyCostLink'
    ,'enggCrm_timesheetLink'
    ,'ecommerce_orderItemLink'
    ,'enggCrm_taskHistoryLink'
    ,'enggCrm_employeeLink'
    ,'ecommerce_product'
    ,'enggCrm_receipt'
);
*/
$hiddenModules = array(
     'core_staffLink'
    ,'payroll_loanRepaymentLink'
);

$tmpName = &$cpCfg['cp.topRooms'];
$cpCfg['cp.availableModules'] = array_merge(
     $tmpName['payroll']['modules']
    ,$tmpName['admin']['modules']
    ,$tmpName['reports']['modules']
    ,$hiddenModules
);

/*
$cpCfg['cp.availableModGroups'] = array(
     'core'
    ,'common'
    ,'project'
    ,'enggCrm'
    ,'crm'
    ,'ecommerce'
    ,'payroll'
);
*/
$cpCfg['cp.availableModGroups'] = array(
     'core'
    ,'common'
    ,'payroll'
);

$cpCfg['cp.availableWidgets'] = array(
     'payroll_ir8aReport'
    ,'payroll_employeePayslipGeneratedReport'
    ,'payroll_employeeSalaryReport'
    ,'payroll_cPFSummaryReport'
    ,'payroll_leaveReport'
    ,'payroll_passportExpiry'
    ,'payroll_workpermitExpiry'
    ,'payroll_employeeSummary'
    ,'payroll_localWorkers'
    ,'payroll_foreignWorkers'
    ,'payroll_allowanceReport'
    ,'payroll_sDLReport'
    ,'payroll_payslipByEmployeeReport'
);

$cpCfg['cp.availablePlugins'] = array(
     'common_comment'
    ,'common_media'
    ,'common_login'
);

$cpCfg['cp.repPrintLogoInRight'] = true;
$cpCfg['cp.repPrintLogoInLeft'] = false;

$cpCfg['cp.assetVersion'] = '20130110';

return $cpCfg;