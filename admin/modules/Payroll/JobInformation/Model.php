<?
class CPL_Admin_Modules_Payroll_JobInformation_Model extends CP_Admin_Modules_Payroll_JobInformation_Model
{
    /**
     *
     */
    function getPrintEmploymentContract() {
        $cpCfg = Zend_Registry::get('cpCfg');
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $media = Zend_Registry::get('media');

        //-----------------------------------------------------------------//
        include_once(CP_LIBRARY_PATH.'lib_php/tbs_us/tbs_class.php');
        include_once(CP_LIBRARY_PATH.'lib_php/tbs_us/plugins/tbs_plugin_opentbs.php');
        include_once(CP_LIBRARY_PATH.'lib_php/tbs_us/plugins/tbs_plugin_html.php');

        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $job_information_id = $fn->getReqParam('job_information_id');
        $jiRec = $fn->getRecordRowByID('job_information', 'job_information_id', $job_information_id);
        $employeeRec = $fn->getRecordRowByID('employee', 'employee_id', $jiRec['employee_id']);
        $countryRec = $fn->getRecordByCondition('geo_country', "country_code = '{$employeeRec['address_country']}'");

        $template = 'Employment Contract.docx';
        $templatePath = $cpCfg['cp.localPath'].'lib/template/' . $template;
        $TBS->LoadTemplate($templatePath);
        $rnd_no = mt_rand();
        $file_name = 'Employee Contract_' . $job_information_id . '_' . $rnd_no . '.docx';
        $file_name = str_replace('.', '_' . date('Y-m-d') . '.', $file_name);

        $path = realpath($cpCfg['cp.mediaFolder']) . '\temp';
        $file_name_save = $path . '\\' . $file_name;
        $sourceFilePath = $file_name_save;
        $today =  date('d/m/Y');
        $todayMonth =  date('dS F Y');
        $todayWithDay =  date('l, d F Y');

        $company_address = strtoupper($cpCfg['cp.addressPdf1']) . ' ' . strtoupper($cpCfg['cp.addressPdf2']) . ' ' . strtoupper($cpCfg['cp.addressPdf4']);
        $employee_address = strtoupper($employeeRec['address_area']) . ' ' . strtoupper($employeeRec['address_street']) . ' ' . strtoupper($countryRec['name']) . ' - ' . $employeeRec['address_po_code'];
        if ($employeeRec['citizen'] == 'Citizen' || $employeeRec['citizen'] == 'PR') {
            $id_no = "NRIC No: {$employeeRec['nric_no']}";
        } else {
            $id_no = "FIN No: {$employeeRec['fin_no']}";
        }

        $valArr = array();
        /* Contact Details */
        $valArr['current_date']         = strtoupper($todayMonth);
        $valArr['company_name']         = strtoupper($cpCfg['cp.companyName']);
        $valArr['company_address']      = $company_address;
        $valArr['employee_name']        = strtoupper($employeeRec['first_name']);
        $valArr['id_no']                = strtoupper($id_no);
        $valArr['designation']          = strtoupper($jiRec['designation']);
        $valArr['duty_responsibility']  = strtoupper($jiRec['duty_responsibility']);
        $valArr['basic_pay']            = number_format($jiRec['basic_pay']);
        $valArr['working_days']         = $jiRec['working_days'];
        $valArr['designation']          = $jiRec['designation'];
        $valArr['employee_address']     = $employee_address;

        $blkMain   = array();
        $blkMain[] = $valArr;

        $TBS->MergeBlock('blkMain', $blkMain);

        $TBS->Show(OPENTBS_DOWNLOAD, $file_name);
    }
}
